<?php

namespace App\Controller;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Attribute\Route;

class MailingController extends AbstractController
{
    #[Route('/api/admin/mailing', methods: ['POST'])]
    public function mailing(Request $request, EntityManagerInterface $entityManager, MailerInterface $mailer): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        if ($user == null || !$user->isAdmin()) {
            return new JsonResponse(
                "Unauthorized request",
                Response::HTTP_UNAUTHORIZED
            );
        }
        $mailingFormData = json_decode($request->getContent());
        if (strlen($mailingFormData->subject) > 100 || strlen($mailingFormData->message) > 500) {
            return new JsonResponse(
                "Bad request value",
                Response::HTTP_BAD_REQUEST
            );
        }
        $planners = $entityManager->getRepository(User::class)->findAllPlanner();
        $addresses = [];
        /** @var User $planner */
        foreach ($planners as $planner) {
            if (count($planner->getCourses()) > 0) {
                $addresses[] = $planner->getEmail();
            }
        }
        $email = (new Email())
            ->from(new Address('noreply@vikazimut.vikazim.fr', 'Vikazimut'))
            ->subject($mailingFormData->subject)
            ->bcc(...$addresses)
            ->text($mailingFormData->message);
        try {
            $mailer->send($email);
        } catch (TransportExceptionInterface) {
            return new JsonResponse(
                "Cannot send mails",
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
        return new JsonResponse();
    }
}