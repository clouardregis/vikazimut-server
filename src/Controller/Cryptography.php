<?php

namespace App\Controller;

class Cryptography
{
    static public function decryptText($encryptedMessage): string
    {
        $encryptVariables = json_decode(file_get_contents((dirname(__DIR__, 2) . '/config/secret/.image.key')), true);
        $key = $encryptVariables["image_file_key"];
        $initializationVector = $encryptVariables["image_file_iv"];
        return self::_decrypt($key, $initializationVector, $encryptedMessage);
    }

    static private function _decrypt($encryptKey, $encryptInitializationVector, $text): string
    {
        $passphrase = base64_decode($encryptKey);
        $initializationVector = base64_decode($encryptInitializationVector);
        $cipherAlgo = 'aes-256-cbc';
        return openssl_decrypt($text, $cipherAlgo, $passphrase, 0, $initializationVector);
    }
}