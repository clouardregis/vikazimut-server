<?php

namespace App\Controller;

use App\Entity\Course;
use App\Entity\Event;
use App\Entity\User;
use App\Model\CreateUser;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Attribute\Route;

class AdminController extends AbstractController
{
    #[Route('/api/admin/download_stats', methods: ['GET'])]
    public function download_stats(EntityManagerInterface $entityManager): Response
    {
        $courses = $entityManager->getRepository(Course::class)->findCoursesWithOrienteerCount(null);
        return PlannerController::createDownloadStats($courses);
    }

    #[Route('/api/admin/planners', methods: ['GET'])]
    public function allPlanners(EntityManagerInterface $entityManager): Response
    {
        /** @var User $user */
        $users = $entityManager->getRepository(User::class)->findAllPlanner();
        $results = [];
        foreach ($users as $user) {
            $results[] = [
                "id" => $user->getId(),
                "username" => $user->getUsername(),
                "email" => $user->getEmail(),
                "courseCount" => count($user->getCourses()),
                "createdDate" => $user->getCreatedDate()->getTimestamp() * 1000,
                "modifiedDate" => $user->getModifiedDate() == null ? -1 : $user->getModifiedDate()->getTimestamp() * 1000,
            ];
        }
        return new JsonResponse(
            $results,
            Response::HTTP_OK
        );
    }

    #[Route('/api/admin/events', methods: ['GET'])]
    public function show_all_events(EntityManagerInterface $entityManager): Response
    {
        /** @var Event $event */
        $events = $entityManager->getRepository(Event::class)->findAll();
        $results = [];
        foreach ($events as $event) {
            $results[] = [
                "id" => $event->getId(),
                "name" => $event->getName(),
            ];
        }
        return new JsonResponse(
            $results,
            Response::HTTP_OK
        );
    }

    #[Route('/api/admin/delete-planner/{planner_id}', requirements: ['planner_id' => '\d+'], methods: ['DELETE'])]
    public function delete_planner(int $planner_id, EntityManagerInterface $entityManager): Response
    {
        $user = $entityManager->getRepository(User::class)->find($planner_id);
        if ($user == null) {
            return new JsonResponse(
                "Unknown user id",
                Response::HTTP_BAD_REQUEST
            );
        }
        $user->remove($entityManager);
        return new JsonResponse();
    }

    #[Route('/api/admin/add-planner', methods: ['POST'])]
    public function add_planner(Request $request, UserPasswordHasherInterface $passwordEncoder, MailerInterface $mailer, EntityManagerInterface $entityManager): Response
    {
        $user = new User();
        $requestData = json_decode($request->getContent());
        $user->setUsername($requestData->username);
        $user->setEmail($requestData->email);
        $user->setPhone($requestData->phone);
        $user->setLastName($requestData->lastname);
        $user->setFirstName($requestData->firstname);
        $user->setCreatedDate(new DateTime("now"));

        $creator = new CreateUser($user);
        $error = $creator->addUser($entityManager, $passwordEncoder);
        if ($error === null) {
            try {
                $message = sprintf($requestData->message, $user->getUsername(), $user->getUsername(), $user->getPlainPassword());
                $creator->sendEmail($mailer, $message);
                return new JsonResponse();
            } catch (TransportExceptionInterface) {
                return new JsonResponse(
                    [
                        "message" => "Cannot send email",
                    ],
                    Response::HTTP_INTERNAL_SERVER_ERROR);
            }
        } else {
            return new JsonResponse(
                $error,
                Response::HTTP_BAD_REQUEST
            );
        }
    }

    #[Route('/api/admin/change-planner-password/{planner_id}', requirements: ['planner_id' => '\d+'], methods: ['PATCH'])]
    public function change_password(int $planner_id, Request $request, UserPasswordHasherInterface $encoder, MailerInterface $mailer, EntityManagerInterface $entityManager): Response
    {
        $user = $entityManager->getRepository(User::class)->find($planner_id);
        if ($user === null) {
            return new JsonResponse(
                [
                    "message" => "Bad request value",
                ],
                Response::HTTP_BAD_REQUEST
            );
        }
        $requestData = json_decode($request->getContent());
        if ($requestData->message === null) {
            return new JsonResponse(
                [
                    "message" => "No message",
                ],
                Response::HTTP_BAD_REQUEST
            );
        }
        $creator = new CreateUser($user);
        $creator->forgotPassword($encoder, $entityManager);
        try {
            $message = sprintf($requestData->message, $user->getUsername(), $user->getPlainPassword(), $user->getUsername(), $user->getPlainPassword());
            $creator->sendEmail($mailer, $message);
            return new JsonResponse();
        } catch (TransportExceptionInterface) {
            return new JsonResponse(
                [
                    "message" => "Cannot send email",
                ],
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
    }
}
