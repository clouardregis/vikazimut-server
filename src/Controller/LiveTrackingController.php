<?php

namespace App\Controller;

use App\Entity\Course;
use App\Entity\LiveTracking;
use App\Entity\LiveTrackingOrienteer;
use App\Entity\User;
use DateTime;
use DateTimeInterface;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class LiveTrackingController extends AbstractController
{
    #[Route('/api/planner/live-tracking/start', methods: ['POST'])]
    public function start_live_tracking(Request $request, EntityManagerInterface $entityManager): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        if ($user == null) {
            return new JsonResponse(
                "Unauthorized request",
                Response::HTTP_UNAUTHORIZED
            );
        }

        $requestData = Cryptography::decryptText($request->getContent());
        $requestContent = json_decode($requestData);
        if ($requestContent == null) {
            return new JsonResponse(
                [
                    "message" => "Unauthorized request",
                ],
                Response::HTTP_UNAUTHORIZED
            );
        }
        $course_id = $requestContent->course_id;
        $now = new DateTime("now");
        $lives = $entityManager->getRepository(LiveTracking::class)->findByCourse($course_id);
        $live = null;
        if (count($lives) > 0) {
            // Clean the database...
            foreach ($lives as $l) {
                /** @var DateTimeInterface $datetime */
                $datetime = $l->getDate();
                $interval = $now->diff($datetime);
                $hours = $interval->h + $interval->days * 24;
                if ($hours > 24) {
                    $entityManager->getRepository(LiveTrackingOrienteer::class)->deleteBy($l);
                    $entityManager->getRepository(LiveTracking::class)->remove($l);
                } else {
                    $live = $l;
                    break;
                }
            }
        }
        $course = $entityManager->getRepository(Course::class)->find($course_id);
        if ($live == null) {
            $live = new LiveTracking();
            $live->setCourse($course);
            $live->setDate($now);
            $live->setCreator($user);
            $isCreator = true;
        } else {
            $isCreator = $live->getCreator() === $user;
        }
        try {
            $entityManager->persist($live);
            $entityManager->flush();
            return new JsonResponse(
                [
                    "liveId" => $live->getId(),
                    "isCreator" => $isCreator,
                ],
                Response::HTTP_OK
            );
        } catch (Exception) {
            return new JsonResponse(
                "Sorry. An internal error has occurred.",
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
    }

    #[Route('/live-tracking/join', methods: ['POST'])]
    public function join_live_tracking(Request $request, EntityManagerInterface $entityManager): Response
    {
        $requestData = Cryptography::decryptText($request->getContent());
        $requestContent = json_decode($requestData);
        if ($requestContent == null) {
            return new JsonResponse(
                [
                    "message" => "Unauthorized request",
                ],
                Response::HTTP_UNAUTHORIZED
            );
        }
        $course_id = $requestContent->course_id;
        $now = new DateTime("now");
        $lives = $entityManager->getRepository(LiveTracking::class)->findByCourse($course_id);
        $live = null;
        if (count($lives) > 0) {
            // Take opportunity to clean the database...
            foreach ($lives as $l) {
                /** @var DateTimeInterface $datetime */
                $datetime = $l->getDate();
                $interval = $now->diff($datetime);
                $hours = $interval->h + $interval->days * 24;
                if ($hours < 24) {
                    $live = $l;
                    break;
                }
            }
        }
        if ($live == null) {
            return new JsonResponse(
                null,
                Response::HTTP_NOT_FOUND
            );
        } else {
            try {
                return new JsonResponse(
                    [
                        "liveId" => $live->getId(),
                    ],
                    Response::HTTP_OK
                );
            } catch (Exception) {
                return new JsonResponse(
                    "Sorry. An internal error has occurred.",
                    Response::HTTP_INTERNAL_SERVER_ERROR
                );
            }
        }
    }

    #[Route('/live-tracking/fetch', methods: ['POST'])]
    public function get_orienteers_positions_live_tracking(Request $request, EntityManagerInterface $entityManager): Response
    {
        $requestData = Cryptography::decryptText($request->getContent());
        $requestContent = json_decode($requestData);
        if ($requestContent == null) {
            return new JsonResponse(
                [
                    "message" => "Unauthorized request",
                ],
                Response::HTTP_UNAUTHORIZED
            );
        }
        $liveId = $requestContent->liveId;
        $orienteerLocations = $entityManager->getRepository(LiveTrackingOrienteer::class)->findByLive($liveId);
        $response = [];
        /** @var LiveTrackingOrienteer $orienteerLocation */
        foreach ($orienteerLocations as $orienteerLocation) {
            if ($orienteerLocation->getLatitudes() != null)
                $response[] = [
                    "id" => $orienteerLocation->getId(),
                    "latitudes" => $orienteerLocation->getLatitudes(),
                    "longitudes" => $orienteerLocation->getLongitudes(),
                    "score" => $orienteerLocation->getScore(),
                    "nickname" => $orienteerLocation->getNickname(),
                    "startTimestamp" => $orienteerLocation->getStartDate()->getTimestamp(),
                ];
        }

        return new JsonResponse(
            $response,
            Response::HTTP_OK
        );
    }

    #[Route('/live-tracking/delete_orienteer', methods: ['DELETE'])]
    public function delete_orienteer_from_live_tracking(Request $request, EntityManagerInterface $entityManager): Response
    {
        $requestData = Cryptography::decryptText($request->getContent());
        $requestContent = json_decode($requestData);
        if ($requestContent == null) {
            return new JsonResponse(
                [
                    "message" => "Unauthorized request",
                ],
                Response::HTTP_UNAUTHORIZED
            );
        }
        $orienteer_id = $requestContent->orienteer_id;
        $orienteer = $entityManager->getRepository(LiveTrackingOrienteer::class)->find($orienteer_id);
        if ($orienteer == null) {
            return new JsonResponse(
                "Not a valid orienteer id",
                Response::HTTP_NOT_FOUND
            );
        } else {
            $entityManager->remove($orienteer);
            $entityManager->flush();
            return new JsonResponse(
                null,
                Response::HTTP_OK
            );
        }
    }

    #[Route('/api/planner/live-tracking/stop', methods: ['POST'])]
    public function stop_live_tracking(Request $request, EntityManagerInterface $entityManager): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        if ($user == null) {
            return new JsonResponse(
                "Unauthorized request",
                Response::HTTP_UNAUTHORIZED
            );
        }

        $requestData = Cryptography::decryptText($request->getContent());
        $requestContent = json_decode($requestData);
        if ($requestContent == null) {
            return new JsonResponse(
                [
                    "message" => "Unauthorized request",
                ],
                Response::HTTP_UNAUTHORIZED
            );
        }
        $liveId = $requestContent->liveId;
        $live = $entityManager->getRepository(LiveTracking::class)->find($liveId);
        if ($live != null && $live->getCreator()->getId() == $user->getId()) {
            $live->remove($entityManager);
        }
        return new JsonResponse();
    }
}
