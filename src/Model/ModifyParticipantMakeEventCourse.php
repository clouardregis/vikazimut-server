<?php

namespace App\Model;

use App\Entity\ParticipantMakeEventCourse;
use App\Entity\Track;
use Doctrine\ORM\EntityManagerInterface;

class ModifyParticipantMakeEventCourse
{
    private ?Track $track = null;

    private ?ParticipantMakeEventCourse $participantMakeEventCourse;

    public function setTrack(?Track $track): self
    {
        $this->track = $track;
        return $this;
    }

    public function setParticipantMakeEventCourse(?ParticipantMakeEventCourse $participantMakeEventCourse): void
    {
        $this->participantMakeEventCourse = $participantMakeEventCourse;
    }

    public function modify(EntityManagerInterface $entityManager): void
    {
        $this->participantMakeEventCourse->setTrack($this->track);
        $this->participantMakeEventCourse->setModified(true);
        $entityManager->persist($this->participantMakeEventCourse);
        $entityManager->flush();
        $this->participantMakeEventCourse->getEventCourse()->getEvent()->updateEventScores($entityManager);
    }
}