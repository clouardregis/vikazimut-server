<?php

namespace App\Model;

use DOMDocument;
use function libxml_use_internal_errors;
use function simplexml_load_string;

class CourseValidator
{
    public static function checkXml(string $xmlContent): ?array
    {
        if ($xmlContent == "") {
            return self::ERROR_XML_EMPTY;
        }
        // allow to "catch" the error
        libxml_use_internal_errors(true);
        $xmlContents = simplexml_load_string($xmlContent);
        if (!$xmlContents) {
            return self::ERROR_XML_INVALID;
        }

        // Check xml version number
        $iofVersion = (string)$xmlContents->attributes()->iofVersion;
        if (!$iofVersion) {
            return self::ERROR_XML_INVALID;
        }
        if (strcmp($iofVersion, "3.0")) {
            return self::ERROR_XML_INVALID_VERSION;
        }

        $xmlData = $xmlContents->RaceCourseData;
        if (!$xmlData) {
            return self::ERROR_XML_INVALID;
        }
        if (!$xmlData->Course) {
            return self::ERROR_XML_NO_COURSE;
        }
        $start = false;
        $finish = false;
        $controlPointName = [];
        foreach ($xmlData->Course->children() as $children) {
            if ($children->getName() === "CourseControl") {
                if ((string)$children["type"] === "Start") {
                    $start = true;
                } elseif ((string)$children["type"] === "Finish") {
                    $finish = true;
                }
                $controlPointName[] = $children->Control;
            }
        }
        if (!($start && $finish)) {
            return self::ERROR_XML_NO_START_END;
        }
        foreach ($xmlData->children() as $children) {
            if ($children->getName() === "Control") {
                $controlPointId = array((string)$children->Id);
                $controlPointName = array_diff($controlPointName, $controlPointId);
            }
        }
        if (count($controlPointName) > 0) {
            return self::ERROR_XML_INVALID_CONTROL_POINT;
        }
        return null;
    }

    public static function checkKml(string $filename): ?array
    {
        if ($filename == "") {
            return self::ERROR_KML_EMPTY;
        }
        $dom = new DOMDocument();
        $dom->loadXML($filename);
        $kmlTag = $dom->getElementsByTagName('LatLonBox');
        if ($kmlTag->length == 0) {
            return self::ERROR_KML_INVALID;
        }
        $northTag = $dom->getElementsByTagName('north');
        $eastTag = $dom->getElementsByTagName('east');
        $southTag = $dom->getElementsByTagName('south');
        $westTag = $dom->getElementsByTagName('west');
        if ($northTag->length == 0 || $southTag->length == 0 || $eastTag->length == 0 || $westTag->length == 0) {
            return self::ERROR_KML_INVALID_POS;
        }
        return null;
    }

    const ERROR_IMAGE_NOT_FOUND = [
        "code" => 2,
        "message" => "Image file is missing"
    ];
    const ERROR_IMAGE_TYPE_INVALID = [
        "code" => 3,
        "message" => "Unknown image format"
    ];
    const ERROR_COURSE_EMPTY = [
        "code" => 6,
        "message" => "Empty course name"
    ];
    const ERROR_IMAGE_TOO_BIG = [
        "code" => 8,
        "message" => "Too big image file"
    ];
    const ERROR_COURSE_DUPLICATE = [
        "code" => 9,
        "message" => "Course name already exist"
    ];
    const ERROR_XML_EMPTY = [
        "code" => 10,
        "message" => "Empty xml file"
    ];
    const ERROR_XML_INVALID = [
        "code" => 11,
        "message" => "Xml structure invalid"
    ];
    const ERROR_XML_NO_COURSE = [
        "code" => 13,
        "message" => "Xml no course defined"
    ];
    const ERROR_XML_NO_START_END = [
        "code" => 14,
        "message" => "Xml no start end"
    ];
    const ERROR_XML_INVALID_CONTROL_POINT = [
        "code" => 15,
        "message" => "Xml invalid control_point"
    ];
    const ERROR_XML_INVALID_LOCATION = [
        "code" => 16,
        "message" => "Invalid xml no location"
    ];
    const ERROR_XML_INVALID_VERSION = [
        "code" => 17,
        "message" => "Xml version invalid"
    ];
    const ERROR_KML_EMPTY = [
        "code" => 20,
        "message" => "Empty kml file"
    ];
    const ERROR_KML_INVALID = [
        "code" => 21,
        "message" => "Kml structure invalid"
    ];
    const ERROR_KML_INVALID_POS = [
        "code" => 22,
        "message" => "Kml position invalid"
    ];
    const ERROR_INVALID_TIME_INTERVAL = [
        "code" => 30,
        "message" => "Incompatible time constraint"
    ];
    const ERROR_INTERNAL = [
        "code" => 100,
        "message" => "Internal error"
    ];
}
