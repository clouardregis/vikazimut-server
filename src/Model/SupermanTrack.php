<?php

namespace App\Model;

use App\Entity\Track;

class SupermanTrack
{
    private int $totalTime;
    private array $punchTimes;
    private array $route;

    public function getTotalTime(): int
    {
        return $this->totalTime;
    }

    public function getPunchTimes(): array
    {
        return $this->punchTimes;
    }

    public function getRoute(): array
    {
        return $this->route;
    }

    /**
     * @param Track[] $tracks
     */
    public function getBestRouteFromTrackSplits(array $tracks): void
    {
        list($routes, $controlPoints, $numberOfLegs) = $this->selectCompletePresetOrderTracks($tracks);
        $this->route = [];
        $this->punchTimes = [
            [
                "punchTime" => 0,
                "controlPoint" => 0,
            ],
        ];
        $baseTime = 0;
        $minTime = 0;
        $maxTime = 0;
        for ($legIndex = 0; $legIndex < $numberOfLegs; $legIndex++) {
            $minLegDuration = PHP_INT_MAX;
            $minLegIndex = -1;
            // Calculate min and max leg times among all routes
            for ($j = 0; $j < count($routes); $j++) {
                $legStartPunchTime = $controlPoints[$j][$legIndex]["punchTime"];
                $legEndPunchTime = $controlPoints[$j][$legIndex + 1]["punchTime"];
                if (($legEndPunchTime - $legStartPunchTime) < $minLegDuration) {
                    $minLegDuration = $legEndPunchTime - $legStartPunchTime;
                    $minTime = $legStartPunchTime;
                    $maxTime = $legEndPunchTime;
                    $minLegIndex = $j;
                }
            }

            // Case of no completed routes
            if ($minLegIndex < 0) {
                $this->route = [];
                $this->totalTime = 0;
                $this->punchTimes = [];
                return;
            }

            // Build best route leg with leg of route $minLegIndex  
            $route = $routes[$minLegIndex];
            $shift = $minTime - $baseTime;
            $this->buildLegRouteWithBestLeg($route, $minTime, $maxTime, $shift);

            // Build related punchTime of best route
            $baseTime = $maxTime - $shift;
            $this->punchTimes [] = [
                "punchTime" => $baseTime,
                "controlPoint" => $legIndex + 1
            ];
        }
        $this->totalTime = $baseTime;
    }

    /**
     * @param Track[] $tracks
     */
    public function selectCompletePresetOrderTracks(array $tracks): array
    {
        /* Route: list of waypoints: [latitude, longitude, timeInMs, elevation] */
        $routes = [];
        /* ControlPoint: list of integers, -1 for unpunched and t >= 0 for punched */
        $controlPoints = [];
        $numberOfLegs = 0;
        foreach ($tracks as $track) {
            if ($track->getFormat() == PRESET_ORDER) {
                $punches = $track->getPunches();
                if ($this->isCompleteAndNotForcedTrack($punches)) {
                    $routes[] = Track::extractWaypointsFromRouteString($track->getTrace());
                    $controlPoints[] = $punches;
                    $numberOfLegs = max($numberOfLegs, count($controlPoints[0]) - 1);
                }
            }
        }
        return array($routes, $controlPoints, $numberOfLegs);
    }

    public static function isCompleteAndNotForcedTrack(?array $punchTimes): bool
    {
        if (count(($punchTimes)) == 0) {
            return false;
        }
        for ($i = 1; $i < count($punchTimes); $i++) {
            $punchTime = $punchTimes[$i];
            $isForced = $punchTime["forced"] ?? false;
            if ($punchTime["punchTime"] <= 0 || $isForced) {
                return false;
            }
        }
        return true;
    }

    public function buildLegRouteWithBestLeg($route, int $minTime, int $maxTime, int $shift): void
    {
        for ($i = 0; $i < count($route); $i++) {
            $waypoint = $route[$i];
            $timestamp = $waypoint[2];
            if ($timestamp >= $minTime && $timestamp <= $maxTime) {
                $waypoint[2] -= $shift;
                $this->route[] = $waypoint;
            }
        }
    }
}