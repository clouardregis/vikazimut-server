<?php

namespace App\Repository;

use App\Entity\Course;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Course|null find($id, $lockMode = null, $lockVersion = null)
 * @method Course|null findOneBy(array $criteria, array $orderBy = null)
 * @method Course[]    findAll()
 * @method Course[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CourseRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Course::class);
    }

    public function findCoursesWithOrienteerCount(?User $user): array
    {
        $courses = array();
        if ($user != null) {
            $query = $this->getEntityManager()->createQuery('
            SELECT c, COUNT(o)
            FROM App\Entity\Course c
            LEFT JOIN c.orienteer o
            WHERE c.creator = :user
            GROUP BY c.id')->setParameter('user', $user->getId());
        } else {
            $query = $this->getEntityManager()->createQuery('
            SELECT c, COUNT(o)
            FROM App\Entity\Course c
            LEFT JOIN c.orienteer o
            GROUP BY c.id');
        }
        $results = $query->getResult();
        foreach ($results as $result) {
            /** @var Course $course */
            $course = $result[0];
            $course->setOrienteerCount($result[1]);
            $courses[] = $course;
        }
        return $courses;
    }
}