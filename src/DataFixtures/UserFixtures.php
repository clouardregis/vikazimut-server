<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\User;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixtures extends Fixture
{
    private UserPasswordHasherInterface $encoder;
    public const TEST_USER_REFERENCE = 'user_test';

    public function __construct(UserPasswordHasherInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager): void
    {
        $user = new User();
        $user->setUsername("test");
        $password = $this->encoder->hashPassword($user, "test");
        $user->setPassword($password);
        $user->setEmail("t@t.t");
        $manager->persist($user);
        $this->addReference(self::TEST_USER_REFERENCE, $user);
        $manager->flush();
    }
}
