<?php

namespace App\Entity;

use App\Repository\LiveTrackingRepository;
use DateTimeInterface;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: LiveTrackingRepository::class)]
class LiveTracking
{
    /** @noinspection PhpPropertyOnlyWrittenInspection */
    #[ORM\Column]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    private ?int $id;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?DateTimeInterface $date;

    #[ORM\ManyToOne(targetEntity: Course::class)]
    private ?Course $course;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'courses')]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $creator;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getCourse(): ?Course
    {
        return $this->course;
    }

    public function setCourse(?Course $course): self
    {
        $this->course = $course;

        return $this;
    }

    public function getCreator(): ?User
    {
        return $this->creator;
    }

    public function setCreator(?User $creator): self
    {
        $this->creator = $creator;

        return $this;
    }

    public function remove(EntityManagerInterface $entityManager): void
    {
        $entityManager->getRepository(LiveTrackingOrienteer::class)->deleteBy($this);
        $entityManager->getRepository(LiveTracking::class)->remove($this, true);
    }
}
