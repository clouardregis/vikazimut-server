<?php

namespace App\Tests\Entity;

use App\Entity\EventCourse;
use App\Entity\ParticipantMakeEventCourse;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class EventCourseTest extends KernelTestCase
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    public function testRemove()
    {
        $eventCourse = $this->entityManager->getRepository(EventCourse::class)->findAll()[0];
        $nbParticipantMakeEventCourse = count($this->entityManager->getRepository(ParticipantMakeEventCourse::class)->findAll());
        $eventCourse->remove($this->entityManager);
        $this->assertSame($nbParticipantMakeEventCourse, count($this->entityManager->getRepository(ParticipantMakeEventCourse::class)->findAll()) + 2);
    }

    public function testGenerateParticipantMakeEventCourse()
    {
        $nbParticipantMakeEventCourse = count($this->entityManager->getRepository(ParticipantMakeEventCourse::class)->findAll());
        $eventCourse = $this->entityManager->getRepository(EventCourse::class)->findAll()[0];
        $participant1 = $eventCourse->getEvent()->getParticipants()[0];
        $participant2 = $eventCourse->getEvent()->getParticipants()[1];
        var_dump($nbParticipantMakeEventCourse);
        var_dump($participant1->getId());
        var_dump($participant2->getId());
        $this->entityManager->getRepository(ParticipantMakeEventCourse::class)->find(array("eventCourse" => $eventCourse, "participant" => $participant1))?->remove($this->entityManager);
        $this->entityManager->getRepository(ParticipantMakeEventCourse::class)->find(array("eventCourse" => $eventCourse, "participant" => $participant2))?->remove($this->entityManager);
        $eventCourse->generateParticipantMakeEventCourse($this->entityManager);
        $this->assertSame($nbParticipantMakeEventCourse, count($this->entityManager->getRepository(ParticipantMakeEventCourse::class)->findAll()));
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        $this->entityManager->close();
        $this->entityManager = null;
    }
}